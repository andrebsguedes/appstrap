import Low from 'lowdb'
import FileAsync from 'lowdb/adapters/FileAsync'
import fsx from 'fs-extra'
import path from 'path'

// const __dirname = process.cwd()

const resolve = path.resolve
const autoInit = true

fsx.ensureDirSync(resolve(__dirname, '../../live'))

let db = null

async function init () {
  db = await new Low(new FileAsync(resolve(__dirname, '../../live/db.json')))

  // Seed an empty DB
  await db.defaults({
    messages: [],
    uploads: []
  }).write()
}

if (autoInit && !db) {
  init()
}

export { init, db }
