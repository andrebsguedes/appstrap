import shortid from 'shortid'

export default {
  Counter: {
    countStr: counter => `Current count: ${counter.count}`
  },

  Query: {
    async hello (root, { name }) {
      return `Hello ${name || 'World'}!`
    },
    async messages (root, args, { db }) {
      return db.get('messages').value()
    },
    async uploads (root, args, { db }) {
      return db.get('uploads').value()
    }
  },

  Mutation: {
    async messageAdd (root, { input }, { pubsub, db }) {
      const message = {
        id: shortid.generate(),
        text: input.text
      }

      await db
        .get('messages')
        .push(message)
        .last()
        .write()

      pubsub.publish('messages', { messageAdded: message })

      return message
    },

    async singleUpload (root, { file }, { processUpload }) {
      return processUpload(file)
    },
    async multipleUpload (root, { files }, { processUpload }) {
      return Promise.all(files.map(processUpload))
    }
  },

  Subscription: {
    counter: {
      subscribe (parent, args, { pubsub }) {
        const channel = Math.random().toString(36).substring(2, 15) // random channel name
        let count = 0
        setInterval(() => pubsub.publish(
          channel,
          {
            // eslint-disable-next-line no-plusplus
            counter: { count: count++ }
          }
        ), 2000)
        return pubsub.asyncIterator(channel)
      }
    },

    messageAdded: {
      subscribe (parent, args, { pubsub }) {
        pubsub.asyncIterator('messages')
      }
    }
  }
}
