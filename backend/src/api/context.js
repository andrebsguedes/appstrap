import { db } from '../db'
import { processUpload } from '../upload'

// Context passed to all resolvers (third argument)
// req => Query
// connection => Subscription
// eslint-disable-next-line no-unused-vars
function context (req, connection) {
  return {
    db,
    processUpload
  }
}

export default context
