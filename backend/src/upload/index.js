import fsx from 'fs-extra'
import shortid from 'shortid'
import { db } from '../db'
import Path from 'path'
const { createWriteStream, ensureDirSync } = fsx
// const __dirname = process.cwd()

const uploadDir = Path.resolve(__dirname, '../../live/uploads')

// Ensure upload directory exists
ensureDirSync(uploadDir)

async function storeUpload ({ stream, filename }) {
  const id = shortid.generate()
  const path = `${uploadDir}/${id}-${filename}`

  return new Promise((resolve, reject) =>
    stream
      .pipe(createWriteStream(path))
      .on('finish', () => resolve({ id, path }))
      .on('error', reject)
  )
}

async function recordFile (file) {
  return db
    .get('uploads')
    .push(file)
    .last()
    .write()
}

async function processUpload (upload) {
  const { stream, filename, mimetype, encoding } = await upload
  const { id, path } = await storeUpload({ stream, filename })
  return recordFile({ id, filename, mimetype, encoding, path })
}

export {
  processUpload
}
