import { IntrospectionFragmentMatcher } from 'apollo-cache-inmemory'

const fragmentMatcher = new IntrospectionFragmentMatcher({
  introspectionQueryResultData: {
    __schema: {
      types: [
        {
          kind: 'INTERFACE',
          name: 'Challenge',
          possibleTypes: [
            { name: 'MultipleChallenge' },
            { name: 'SimpleChallenge' }
          ]
        },
        {
          kind: 'INTERFACE',
          name: 'Document',
          possibleTypes: [
            { name: 'Image' },
            { name: 'Text' }
          ]
        },
        {
          kind: 'INTERFACE',
          name: 'Annotation',
          possibleTypes: [
            { name: 'Classification' },
            { name: 'ImageSegment' },
            { name: 'TextSegment' }
          ]
        }
      ]
    }
  }
})

export default fragmentMatcher
